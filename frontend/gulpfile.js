var gulp = require('gulp'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    LessAutoprefix = require('less-plugin-autoprefix'),
    autoprefix = new LessAutoprefix({ browsers: ['Firefox < 20', 'ie 6-11', 'Chrome < 20','Opera < 20'] }),
    cssmin = require('gulp-cssmin');


gulp.task('less', function () {
    gulp.src('./source/less/style.less')
        .pipe(less({
            plugins: [autoprefix]
        }))
        .pipe(rename('production.css'))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('cssmin', function () {
    gulp.src('./dist/css/production.css')
        .pipe(cssmin())
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('default', function () {
    gulp.run('less');

    gulp.watch("./source/less/*.less", function (event) {
        gulp.run('less');
    });
});

gulp.task('build', function () {
    gulp.run('cssmin');
});