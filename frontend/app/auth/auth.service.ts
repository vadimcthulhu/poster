
export class AuthService {
    constructor(){}
    login(credits) {
        window.localStorage.setItem('login', credits.login);
    }
    logout() {
        window.localStorage.removeItem('login');
    }
    authCheck() {
        return window.localStorage.getItem('login');
    }
}