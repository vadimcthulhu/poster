"use strict";
var AuthService = (function () {
    function AuthService() {
    }
    AuthService.prototype.login = function (credits) {
        window.localStorage.setItem('login', credits.login);
    };
    AuthService.prototype.logout = function () {
        window.localStorage.removeItem('login');
    };
    AuthService.prototype.authCheck = function () {
        return window.localStorage.getItem('login');
    };
    return AuthService;
}());
exports.AuthService = AuthService;
