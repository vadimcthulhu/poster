import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { IPost } from "./post.model";
import { API_URL } from "../global";


@Injectable()

export class PostService {



    constructor(private _http: Http) {
        
    }

    ngOnInit() {
    }



    getUserPosts(userid): Observable<IPost[]> {
        return this._http.get(`${API_URL}api/post?filter[where][user]=${userid}`)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}