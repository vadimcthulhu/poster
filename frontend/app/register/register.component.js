"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var register_service_1 = require("../register/register.service");
var auth_service_1 = require("../auth/auth.service");
var Register = (function () {
    function Register(RegisterService, auth) {
        this.RegisterService = RegisterService;
        this.auth = auth;
        this.model = { name: '', username: '', password: '' };
    }
    Register.prototype.onRegister = function () {
        this.formerrors = '';
        if (this.RegisterService.onRegister(this.model)) {
            // юзер не занят, можно делать действия после регистрации
            this.formerrors = 'Register is successful';
            this.model = { name: '', username: '', password: '' };
        }
        else {
            // такой логин уже используется, выводим ошибку
            this.formerrors = 'This login already used';
        }
    };
    Register.prototype.authCheck = function () {
        if (this.auth.authCheck()) {
            return this.auth.authCheck();
        }
        else {
            return false;
        }
    };
    Register = __decorate([
        core_1.Component({
            selector: 'register',
            templateUrl: './app/register/register.component.html',
            providers: [register_service_1.RegisterService, auth_service_1.AuthService]
        }), 
        __metadata('design:paramtypes', [register_service_1.RegisterService, auth_service_1.AuthService])
    ], Register);
    return Register;
}());
exports.Register = Register;
