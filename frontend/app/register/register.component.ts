import { Component } from '@angular/core';

import { RegisterService } from "../register/register.service";
import { AuthService } from "../auth/auth.service";

@Component({
    selector: 'register',
    templateUrl: './app/register/register.component.html',
    providers: [RegisterService, AuthService]
})

export class Register {
    model = { name:'', username: '', password: '' };
    formerrors: string;
    
    constructor(private RegisterService: RegisterService, private auth: AuthService) {
    }

    onRegister() {
        this.formerrors = '';
        if (this.RegisterService.onRegister(this.model)) {
            // юзер не занят, можно делать действия после регистрации
            this.formerrors = 'Register is successful';
            this.model = {name:'',username:'',password:''}
        } else {
            // такой логин уже используется, выводим ошибку
            this.formerrors = 'This login already used';
        }
    }

    authCheck() {
        if (this.auth.authCheck()) {
            return this.auth.authCheck();
        } else {
            return false;
        }
    }


}