import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { User } from "../users/users.model";
import { API_URL } from "../global";


@Injectable()

export class RegisterService {
    tempuser: User;
    

    constructor(private _http: Http) {

    }

    ngOnInit() {
    }

    getUserByLogin(username): Observable<User> {
        return this._http.get(`${API_URL}api/user?filter[where][username]=${username}`)
            .map(this.extractData)
            .catch(this.handleError);
    }
    
    onRegister(credits){
        this.getUserByLogin(credits.username).subscribe((user) => { this.tempuser = user });
        if (this.tempuser) {
            return 0;
        } else {
            let body = JSON.stringify(credits);
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            this._http.post(`${API_URL}api/user`, body, options)
                .map(response => response.json())
                .subscribe();
            return 1;
        }
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}