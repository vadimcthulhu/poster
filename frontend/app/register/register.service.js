"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var global_1 = require("../global");
var RegisterService = (function () {
    function RegisterService(_http) {
        this._http = _http;
    }
    RegisterService.prototype.ngOnInit = function () {
    };
    RegisterService.prototype.getUserByLogin = function (username) {
        return this._http.get(global_1.API_URL + "api/user?filter[where][username]=" + username)
            .map(this.extractData)
            .catch(this.handleError);
    };
    RegisterService.prototype.onRegister = function (credits) {
        var _this = this;
        this.getUserByLogin(credits.username).subscribe(function (user) { _this.tempuser = user; });
        if (this.tempuser) {
            return 0;
        }
        else {
            var body = JSON.stringify(credits);
            var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
            var options = new http_1.RequestOptions({ headers: headers });
            this._http.post(global_1.API_URL + "api/user", body, options)
                .map(function (response) { return response.json(); })
                .subscribe();
            return 1;
        }
    };
    RegisterService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    RegisterService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Rx_1.Observable.throw(errMsg);
    };
    RegisterService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], RegisterService);
    return RegisterService;
}());
exports.RegisterService = RegisterService;
