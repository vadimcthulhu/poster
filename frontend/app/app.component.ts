import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { UsersComponent } from "./users/users.component";
import { MenuService } from "./menu/menu.service";
import { IMenu } from "./menu/menu.model";
import { Home } from "./home/home.component";
import { Login } from "./login/login.component";
import { UsersList } from "./users/UsersList/userslist.component";
import { UserDetail } from "./users/UserDetail/userdetail.component";
import { Register } from "./register/register.component";

@Component({
    selector: 'app',
    directives: [UsersComponent, ROUTER_DIRECTIVES],
    templateUrl: './app/app.component.html',
    providers: [MenuService],
    precompile: [Home, Login, UsersList, UserDetail, Register]
})



export class AppComponent {
    menulist: IMenu[];
    
    constructor(private menu: MenuService) {
        
        this.menulist = menu.getMenu();
    }

}