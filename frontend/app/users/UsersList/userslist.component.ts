import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { User } from "../users.model";
import { UsersService } from "../users.service";

@Component({
    selector: 'user',
    templateUrl: './app/users/UsersList/userslist.component.html',
    providers: [ UsersService ],
    directives: [ ROUTER_DIRECTIVES ]
})
export class UsersList {

    users: User[];

    constructor(private UsersService: UsersService) {

    }

    ngOnInit() {
        this.UsersService.getUsers().subscribe(
            users => this.users = users);
    }
}