import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { User } from "./users.model";
import {API_URL} from "../global";


@Injectable()

export class UsersService {
    users: User[];



    constructor(private _http: Http) {
    }
    
    ngOnInit() {
    }
    
    getUser(id): Observable<User> {
        return this._http.get(`${API_URL}api/user/${id}`)
            .map(this.extractData)
            .catch(this.handleError);
    }
    
    getUsers(): Observable<User[]> {
        return this._http.get(`${API_URL}api/user`)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}