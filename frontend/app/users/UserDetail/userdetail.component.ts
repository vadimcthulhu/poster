import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UsersService } from "../users.service";
import { User } from "../users.model";
import { IPost } from "../../posts/post.model";
import { PostService } from "../../posts/post.service";

@Component({
    selector: 'user',
    templateUrl: './app/users/UserDetail/userdetail.component.html',
    providers: [ UsersService, PostService ]
})
export class UserDetail {

    user: User;
    userPosts: IPost[];
    id: number;

    constructor(private route: ActivatedRoute, private UsersService: UsersService, private PostService: PostService) {
    }

    ngOnInit() {
        this.route.params.map(params => params['id']).subscribe((id) => {
                this.id = id;
            });
        this.UsersService.getUser(this.id).subscribe(
            (user) => {
                this.user = user
            });
        this.PostService.getUserPosts(this.id).subscribe(
            (userPosts) => {
                this.userPosts = userPosts;
            });

    }
}
