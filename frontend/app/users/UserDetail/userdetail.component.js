"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var users_service_1 = require("../users.service");
var post_service_1 = require("../../posts/post.service");
var UserDetail = (function () {
    function UserDetail(route, UsersService, PostService) {
        this.route = route;
        this.UsersService = UsersService;
        this.PostService = PostService;
    }
    UserDetail.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.map(function (params) { return params['id']; }).subscribe(function (id) {
            _this.id = id;
        });
        this.UsersService.getUser(this.id).subscribe(function (user) {
            _this.user = user;
        });
        this.PostService.getUserPosts(this.id).subscribe(function (userPosts) {
            _this.userPosts = userPosts;
        });
    };
    UserDetail = __decorate([
        core_1.Component({
            selector: 'user',
            templateUrl: './app/users/UserDetail/userdetail.component.html',
            providers: [users_service_1.UsersService, post_service_1.PostService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, users_service_1.UsersService, post_service_1.PostService])
    ], UserDetail);
    return UserDetail;
}());
exports.UserDetail = UserDetail;
