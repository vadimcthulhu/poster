"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var global_1 = require("../global");
var UsersService = (function () {
    function UsersService(_http) {
        this._http = _http;
    }
    UsersService.prototype.ngOnInit = function () {
    };
    UsersService.prototype.getUser = function (id) {
        return this._http.get(global_1.API_URL + "api/user/" + id)
            .map(this.extractData)
            .catch(this.handleError);
    };
    UsersService.prototype.getUsers = function () {
        return this._http.get(global_1.API_URL + "api/user")
            .map(this.extractData)
            .catch(this.handleError);
    };
    UsersService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    UsersService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Rx_1.Observable.throw(errMsg);
    };
    UsersService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], UsersService);
    return UsersService;
}());
exports.UsersService = UsersService;
