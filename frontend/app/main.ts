import { bootstrap }    from '@angular/platform-browser-dynamic';
import { APP_ROUTER_PROVIDERS } from './app.routes';
import { HTTP_PROVIDERS } from '@angular/http';
import { LocationStrategy, HashLocationStrategy }from '@angular/common';

import { AppComponent } from './app.component';
import { enableProdMode } from '@angular/core';

enableProdMode();


bootstrap(AppComponent, [
    APP_ROUTER_PROVIDERS, HTTP_PROVIDERS, {provide: LocationStrategy, useClass: HashLocationStrategy}
]).catch(err => console.error(err));