"use strict";
var router_1 = require('@angular/router');
var userdetail_component_1 = require("./users/UserDetail/userdetail.component");
var userslist_component_1 = require("./users/UsersList/userslist.component");
var home_component_1 = require("./home/home.component");
var login_component_1 = require("./login/login.component");
var register_component_1 = require("./register/register.component");
exports.routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    { path: 'home', component: home_component_1.Home },
    { path: 'login', component: login_component_1.Login },
    { path: 'register', component: register_component_1.Register },
    { path: 'users', component: userslist_component_1.UsersList },
    { path: 'profile/:id', component: userdetail_component_1.UserDetail }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
