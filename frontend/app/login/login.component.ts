import { Component } from '@angular/core';
import { AuthService } from "../auth/auth.service";

@Component({
    selector: 'login',
    templateUrl: './app/login/login.component.html',
    providers: [AuthService]
})

export class Login {
    model = { login: '', password: '' };
    constructor(private auth: AuthService) {
        
    }
    
    onLogin() {
        this.auth.login(this.model);
    }

    authCheck() {
        if (this.auth.authCheck()) {
            return this.auth.authCheck();
        } else {
            return false;
        }
    }
    logout() {
        this.auth.logout();
    }
    
}