export class MenuService {    
    getMenu (){
        return [
            { name:'Home', path: '/home' },
            { name:'Login', path: '/login' },
            { name:'Register', path: '/register' },
            { name:'Users', path: '/users' }
        ]
    }
}