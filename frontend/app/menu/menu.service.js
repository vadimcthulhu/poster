"use strict";
var MenuService = (function () {
    function MenuService() {
    }
    MenuService.prototype.getMenu = function () {
        return [
            { name: 'Home', path: '/home' },
            { name: 'Login', path: '/login' },
            { name: 'Register', path: '/register' },
            { name: 'Users', path: '/users' }
        ];
    };
    return MenuService;
}());
exports.MenuService = MenuService;
