import { provideRouter, RouterConfig } from '@angular/router';


import { UserDetail } from "./users/UserDetail/userdetail.component";
import { UsersList } from "./users/UsersList/userslist.component";
import { Home } from "./home/home.component";
import { Login } from "./login/login.component";
import { Register } from "./register/register.component";


export const routes: RouterConfig = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    { path: 'home', component: Home },
    { path: 'login', component: Login },
    { path: 'register', component: Register },
    { path: 'users', component: UsersList },
    { path: 'profile/:id', component: UserDetail }
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];