"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var users_component_1 = require("./users/users.component");
var menu_service_1 = require("./menu/menu.service");
var home_component_1 = require("./home/home.component");
var login_component_1 = require("./login/login.component");
var userslist_component_1 = require("./users/UsersList/userslist.component");
var userdetail_component_1 = require("./users/UserDetail/userdetail.component");
var register_component_1 = require("./register/register.component");
var AppComponent = (function () {
    function AppComponent(menu) {
        this.menu = menu;
        this.menulist = menu.getMenu();
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            directives: [users_component_1.UsersComponent, router_1.ROUTER_DIRECTIVES],
            templateUrl: './app/app.component.html',
            providers: [menu_service_1.MenuService],
            precompile: [home_component_1.Home, login_component_1.Login, userslist_component_1.UsersList, userdetail_component_1.UserDetail, register_component_1.Register]
        }), 
        __metadata('design:paramtypes', [menu_service_1.MenuService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
